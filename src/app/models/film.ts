export interface GetFilmParams {
  start?: number;
  end?: number;
  data: number;
  format: string;
}

export interface Trailer {
  quality: string;
  videoURL: string;
}

export interface Film {
  countries: string[];
  directors: {id: string; name: string}[];
  genres: string[];
  idIMDB: string;
  languages: string[];
  metascore: number;
  plot: string;
  rated: string;
  rating: number;
  releaseDate: string;
  runtime: number;
  simplePlot: string;
  title: string;
  type: string;
  urlIMDB: string;
  urlPoster: string;
  votes: number;
  writers: {id: string; name: string}[];
  year: number;
}
