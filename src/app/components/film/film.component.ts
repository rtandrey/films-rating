import { Component, OnInit, Input } from '@angular/core';
import { apiItems } from '../../constants/api.constants';
import { DataStorageService } from '../../services/data-storage/data-storage.service';
import { Film, Trailer } from '../../../app/models/film';
import { get } from 'lodash';
import { Observable, tap } from 'rxjs';
import { TrailerComponent } from '../trailer/trailer.component';
import { SelectedFilmsButtonsComponent } from '../selected-films-buttons/selected-films-buttons.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss'],
  imports: [TrailerComponent, SelectedFilmsButtonsComponent, TrailerComponent, CommonModule],
  providers: [DataStorageService],
  standalone: true,
})
export class FilmComponent implements OnInit {
  @Input() film!: Film;
  @Input() isOwnTop = false;
  directorExtLink: string = apiItems.films.director;
  trailersObs!: Observable<Trailer[]>;
  constructor(private filmService: DataStorageService) { }

  ngOnInit(): void {
    this.initTrailers();
  }

  initTrailers() {
    const idIMDB = get(this.film, 'idIMDB');
    if (idIMDB) {
      // this.trailersObs = this.filmService.getTrailers(idIMDB).pipe(
      //   tap(data => console.log('DATA: ', data, idIMDB)
      //   )
      // );
    }
  }

}
