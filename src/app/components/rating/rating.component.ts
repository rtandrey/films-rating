import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../../services/data-storage/data-storage.service';
import { Observable } from 'rxjs';
import { Film } from '../../models/film';
import { FilmComponent } from '../film/film.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
  imports: [FilmComponent, CommonModule],
  providers: [DataStorageService], 
  standalone: true,
})
export class RatingComponent implements OnInit {
  topFilmsObs!: Observable<Film[]>;
  isOwnTop = false;

  constructor(private filmService: DataStorageService) { }

  ngOnInit(): void {
    this.topFilmsObs = this.filmService.getComingSoonFilms();
  }

}
