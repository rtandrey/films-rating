// import { ChartOptions, ChartType } from 'chart.js';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
// import * as pluginDataLabels from 'chartjs-plugin-datalabels';
// import { Label } from 'ng2-charts';
import { DataStorageService } from '../../services/data-storage/data-storage.service';
import { map, get, sortBy, filter } from 'lodash';
import { Film } from '../../models/film';
import { CommonModule } from '@angular/common';
import Chart from 'chart.js/auto';
import 'zone.js';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss'],
  standalone: true,
})
export class PieChartComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas') canvas!: ElementRef<any>;
  public chart: any;
//   public pieChartOptions: ChartOptions = {
//     responsive: true,
//     // legend: {
//     //   position: 'top',
//     // },
//   };
//   public pieChartLabels: Label[] = [];
//   public pieChartData: number[] = [];
//   public pieChartType: ChartType = 'pie';
//   public pieChartLegend = true;
//   public pieChartPlugins = [
//     pluginDataLabels
//     // {
//     // afterLayout(chart) {
//     //   chart.legend.legendItems.forEach(
//     //     (label) => {
//     //       label.text = label.text[0];
//     //       return label;
//     //     }
//     //   );
//     // }
//   // }
// ];
//   public pieChartColors = [
//     {
//       backgroundColor: [] as string[],
//     },
//   ];
//   public yearsSet = new Set();

//   constructor(private filmService: DataStorageService) { }

  ngOnInit(): void {
//     this.initPieData();
  }

  ngAfterViewInit(): void {
    this.chart = new Chart(this.canvas.nativeElement, {
      type: 'pie',
      data: {
        labels: ['I', 'II','III','IV'],
	        datasets: [{
            label: 'Films by decade',
            data: [300, 240, 100, 432],
            backgroundColor: [
              'red',
              'pink',
              'green',
              'yellow',		
            ],
            hoverOffset: 4
          }],
      },
      options: {
        aspectRatio: 2.5
      }

    });
  }

//   initPieData(): void {
//     this.filmService.getTopFilms().subscribe(topFilms => {
//       const sortedFilmsByYear = sortBy(topFilms, ['year']);
//       const allFilms = map(sortedFilmsByYear, (film) => {
//         return this.setYearDecade(film);
//       });
//       [...this.yearsSet].forEach(year => {
//         this.initPieChartData(year, allFilms);
//         this.initPieChartLabels(year, allFilms);
//         this.initPieChartColors();
//       });
//     });
//   }

//   setYearDecade(film: Film) {
//     const year = parseInt(get(film, 'year', ''), 10);
//     const decade = Math.trunc( year / 10) * 10;
//     film.decade = decade;
//     this.yearsSet.add(decade);
//     return film;
//   }

//   initPieChartData(year: string, allFilms: Film[]) {
//     const numberFilmsByDecade = filter(allFilms, ['decade', year]).length;
//     this.pieChartData.push(numberFilmsByDecade);
//   }

//   initPieChartLabels(year: string, allFilms: Film[]) {
//     const filmsByDecade = filter(allFilms, ['decade', year])
//       .map((film) => {
//         return `${film.title}, (${film.year})`;
//       });
//     this.pieChartLabels.push([year, ...filmsByDecade]);
//   }

//   initPieChartColors() {
//     this.pieChartColors[0].backgroundColor.push(this.getRandomColor(0.3));
//   }

//   getRandomColor(opacity?: number): string {
//     const red = Math.floor(Math.random() * 256);
//     const green = Math.floor(Math.random() * 256);
//     const blue = Math.floor(Math.random() * 256);
//     const opacityValue = opacity ? opacity : 1;
//     const rgbColor = `rgb(${red},${green},${blue}, ${opacityValue})`;
//     return rgbColor;
//   }

}
