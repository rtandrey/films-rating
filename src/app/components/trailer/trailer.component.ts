import { Component, OnInit, Input } from '@angular/core';
import { Trailer } from '../../models/film';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ModalVideoComponent } from '../modal-video/modal-video.component';

@Component({
  selector: 'app-trailer',
  templateUrl: './trailer.component.html',
  styleUrls: ['./trailer.component.scss'],
  standalone: true,
})
export class TrailerComponent implements OnInit {
  @Input() trailer!: Trailer;
  @Input() title!: string;
  constructor(private modalService: BsModalService) { }

  ngOnInit(): void { }

  showTrailer() {
    const initialState = {
      title: this.title,
      videoURL: this.trailer.videoURL,
    };
    this.modalService.show(ModalVideoComponent, {initialState});
  }

}
