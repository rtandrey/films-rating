import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal-video',
  templateUrl: './modal-video.component.html',
  styleUrls: ['./modal-video.component.scss'],
  standalone: true,
})
export class ModalVideoComponent implements OnInit {
  title = '';
  videoURL = '';
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit(): void { }

}
