import { Component, EventEmitter, Output } from '@angular/core';
import { RouterLink } from '@angular/router';
import { TabsModule, TabsetConfig } from 'ngx-bootstrap/tabs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  imports: [RouterLink, TabsModule ],
  standalone: true,
})
export class HeaderComponent {
  constructor() {}
}
