import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../../services/data-storage/data-storage.service';
import { BrowserStorageService } from '../../services/browser-storage/browser-storage.service';
import { Observable } from 'rxjs';
import { Film } from '../../models/film';
import { FilmComponent } from '../film/film.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-selected-top',
  templateUrl: './selected-top.component.html',
  styleUrls: ['./selected-top.component.scss'],
  imports: [FilmComponent, CommonModule],
  providers: [BrowserStorageService],
  standalone: true,
})
export class SelectedTopComponent implements OnInit {
  isOwnTop = true;
  topFilmsObs!: Observable<Film[]>;

  constructor(
    private dataStorageService: DataStorageService,
    private browserStorageService: BrowserStorageService) { }

  ngOnInit(): void {
    this.initData();
    this.browserStorageService.isFilmRemoved.subscribe((status) => {
      this.initData();
    });
  }

  initData() {
    const allIds = this.getAllId();
    this.topFilmsObs = this.getFilms(allIds);
  }

  getAllId() {
    return this.browserStorageService.getAllLocal();
  }

  getFilms(idsIMDB: string[]) {
    return this.dataStorageService.getFilmsById(idsIMDB);
  }

}
