import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedTopComponent } from './selected-top.component';

describe('SelectedTopComponent', () => {
  let component: SelectedTopComponent;
  let fixture: ComponentFixture<SelectedTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
