import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Component, OnInit, Input } from '@angular/core';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { BrowserStorageService } from '../../services/browser-storage/browser-storage.service';
import { get } from 'lodash';
import { Film } from '../../models/film';

@Component({
  selector: 'app-selected-films-buttons',
  templateUrl: './selected-films-buttons.component.html',
  styleUrls: ['./selected-films-buttons.component.scss'],
  imports: [FontAwesomeModule],
  standalone: true,
})
export class SelectedFilmsButtonsComponent implements OnInit {
  @Input() film!: Film;
  @Input() showButtons = true;
  @Input() showRemove = false;
  idIMDB = '';
  faThumbsUp = faThumbsUp;
  isPrefered = false;

  constructor(private browserStorageService: BrowserStorageService) { }

  ngOnInit(): void {
    this.initData();
  }

  removeFromPreferred() {
    this.browserStorageService.removeLocal(this.idIMDB);
    this.initData();
  }

  initData() {
    this.idIMDB = get(this.film, 'idIMDB', '');
    if (this.idIMDB) {
    const localId = this.browserStorageService.getLocal(this.idIMDB);
    this.isPrefered = !!localId;
    }
  }

  addToPreferred() {
    this.browserStorageService.setLocal(this.idIMDB, this.idIMDB);
    this.initData();
  }

}
