import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedFilmsButtonsComponent } from './selected-films-buttons.component';

describe('SelectedFilmsButtonsComponent', () => {
  let component: SelectedFilmsButtonsComponent;
  let fixture: ComponentFixture<SelectedFilmsButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedFilmsButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedFilmsButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
