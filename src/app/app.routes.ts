import { Routes } from '@angular/router';
import { RatingComponent } from './components/rating/rating.component';
import { SelectedTopComponent } from './components/selected-top/selected-top.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';

export const routes: Routes = [
    {
      path: '',
      redirectTo: 'rating',
      pathMatch: 'full',
    },
    {
      path: 'rating',
      // component: RatingComponent,
      loadComponent: () => import('./components/rating/rating.component').then((m) => m.RatingComponent),
    },
    {
      path: 'selected-top',
      // component: SelectedTopComponent,
      loadComponent: () => import('./components/selected-top/selected-top.component').then((m) => m.SelectedTopComponent),
    },
    {
      path: 'decades',
      component: PieChartComponent,
    },
    { path: '**', redirectTo: 'rating' },
  ];