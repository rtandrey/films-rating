export const apiItems = {
  films: {
    // top: 'https://www.myapifilms.com/imdb/top',
    director: 'https://www.imdb.com/name/',
    trailers: 'https://www.myapifilms.com/imdb/trailers',
    film: 'https://www.myapifilms.com/imdb/idIMDB',
    comingSoon: 'https://www.myapifilms.com/imdb/comingSoon',
  },
};

export const tokenUrl = 'https://www.myapifilms.com';

export const apiToken = ''; // put here your token: https://www.myapifilms.com/token.do
