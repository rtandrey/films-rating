import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilmComponent } from './components/film/film.component';
import { HeaderComponent } from './components/header/header.component';
import { RatingComponent } from './components/rating/rating.component';
import { TrailerComponent } from './components/trailer/trailer.component';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalVideoComponent } from './components/modal-video/modal-video.component';
import { SelectedTopComponent } from './components/selected-top/selected-top.component';
import { SelectedFilmsButtonsComponent } from './components/selected-films-buttons/selected-films-buttons.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpRequestInterceptor } from './interceptors/http/http.interceptor';
import { ErrorsInterceptor } from './interceptors/errors/errors.interceptor';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { provideCharts, withDefaultRegisterables } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    RatingComponent,
    FilmComponent,
    TrailerComponent,
    HeaderComponent,
    ModalVideoComponent,
    SelectedTopComponent,
    SelectedFilmsButtonsComponent,
    PieChartComponent,
  ],
  imports: [
    ModalModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    FontAwesomeModule
  ],
  providers: [
    // BsModalRef.,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: HttpRequestInterceptor,
    //   multi: true,
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorsInterceptor,
      multi: true,
    },
    // provideCharts(withDefaultRegisterables())
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
