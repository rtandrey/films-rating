import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
// import { Subscription, of } from 'rxjs';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, HeaderComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'films-rating';
  // sub!: Subscription;

  constructor() {
    this.title = 'HELLO';
    // this.sub.add(of(1))
    console.log(this.title);
  }
}
