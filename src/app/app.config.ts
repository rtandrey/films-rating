import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { routes } from './app.routes';
import { HttpRequestInterceptor } from './interceptors/http/http.interceptor';
import { ErrorsInterceptor } from './interceptors/errors/errors.interceptor';

import { provideCharts, withDefaultRegisterables } from 'ng2-charts';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(withInterceptors([HttpRequestInterceptor])),
    provideCharts(withDefaultRegisterables())
  ]
};
