import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { apiItems } from '../../constants/api.constants';
import { Film, GetFilmParams } from '../../models/film';
import { get, map as _map, flatten } from 'lodash';
import { combineLatest, map, Observable, of, switchMap } from 'rxjs';

const trailerMock = [
  {
    quality: '480p',
    videoURL: 'https://imdb-video.media-imdb.com/vi3877612057/MV5BODlhNjU1YjQtOTQyOS00MmY2LWFlZTgtODg1ZjY1N2NjNDMyXkExMV5BbXA0XkFpbWRiLWV0cy10cmFuc2NvZGU@.mp4?Expires=1586694556&Signature=U69DP0FqV95yTX89LGhRlWbdqQ7zuqoGfLwlxG7PmJwWhXs3Ih9p-UsQ7eWgW6q7ndcXSHaCBM8ozZbyjCBr3ME5~rhZgRn9TcGhPFI7sl6GcLvx8CHzyfIHVXjWMas1Sg1fg3eNo-2AAi-MDPoW0JM96mFAQnPLfBX14H4pK95h3igR3Z2lo5c4zkA9mmTXvFTJ~maZWnq3aMfWRoaYjOBIT1rx5vOhzbKWkKHyY0TPc2ismVWvhLbKl-yndqw2NoiLuEJazfw1Xno16TjuyrdL-IhfnznGLEkyJ94IbIDsd5nCNNlJbtDGjJT8657IMlhgqoc4iDmsN8dbKlnDtg__&Key-Pair-Id=APKAIFLZBVQZ24NQH3KA',
  },
  {
    quality: 'SD',
    videoURL: 'https://imdb-video.media-imdb.com/vi3877612057/MV5BYjE0YjMxOTgtNjlmYS00NDA5LTgwZWItODFkMGI2NTM0ZmE4XkExMV5BbXA0XkFpbWRiLWV0cy10cmFuc2NvZGU@.mp4?Expires=1586694556&Signature=trn-xaARxlofNwh7qyNKtKwrQRa7hpVQg5TgTkCXHmEBLNKTCWV6Xe9Fa5kTwcyrL1wMJXVQyVWOXGK8yrG8a9KbRCJa5m7E~LIFTZYTrGs9YwZTGwyEZVdKf45RgSCpqUKlh8~grK8zmQCwpKE8AKSETw7OdR~0BJAT4-~GC4wraZhylz-H6RI6GFZmTZnoXMtxQKgNApMuay9u~RSJ211kUC02CGewVpbz896kZ-S7QpCuF9DaPC4slF~qg9c1J-NAq2B0vNVAFccprVa4JaHeOXR6KZuUYmISwvEMmHjRiNhcS99l6opUKeC1vlRvbGld~85NODc79s5Oh6lPTw__&Key-Pair-Id=APKAIFLZBVQZ24NQH3KA',
  },
  {
    quality: 'SD',
    videoURL: 'https://imdb-video.media-imdb.com/vi3877612057/MV5BMGQzZGEyYTQtMTYwMi00MTdhLWFhMWYtYzYxOWIxZDBjNzllXkExMV5Bd2VibV5BaW1kYi1ldHMtdHJhbnNjb2Rl.webm?Expires=1586694556&Signature=d2wregnLELkW55B56AxzaXFuq~-vg2EADBre5HYkXw1nWCHQv2066uCYyJObItSaEFQaERcf~ySnasscDmFIwYWp48llkncXVcm~eesxdZNcnghLbgHkApm1PtRwYdDpUhGqiH46-FwzhhzlLrJa-A0nWYSC5QkHB9i19iqz5dNruEzg5DccIauPWe4j9u~zHc7oIUk0N64HmcBqCMXC285A~z79ShDZwSTy3v0SlZPMeiOi9cDHEF~6AFm9RuMaq2oe83YQwYLiZSiKtp1g3PWVCV7emmjDuBjdI9uA98-rOpNp6W2xNieFhoBFnq33h8fXwVHcIA6x4UKlIVRnjA__&Key-Pair-Id=APKAIFLZBVQZ24NQH3KA',
  }
];

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private http: HttpClient) { }

  getComingSoonFilms(): Observable<Film[]> {
    const params = new HttpParams();
    return this.http.get(apiItems.films.comingSoon, { params })
      .pipe(
        map((filmsData) => {
          return get(filmsData, ['data', 'comingSoon'], []);
        }),
        switchMap((val: {movies: {idIMDB: string; }[]}[]) => {
          const movies = (val[0]?.movies || [] as {idIMDB: string; }[]).map((movie: {idIMDB: string; }) => movie.idIMDB);
          const mas = val.map(film => film.movies[0].idIMDB);
          return this.getFilmsById(mas);
        })
      );
  }
  // getFilms() {
  //   let params = new HttpParams();
  //   params = params.append('data', '1');
  //   params = params.append('end', '20');
  //   return this.http.get(apiItems.films.top, { params })
  //     .pipe(map((filmsData) => {
  //       return get(filmsData, ['data', 'movies'], []);
  //     }));
  // }

  getFilmsById(idsIMDB: string[]): Observable<Film[]> {
    let params = new HttpParams();
    params = params.append('trailers', '1');
    return combineLatest( _map(idsIMDB, (id => {
      params = params.set('idIMDB', id);
      return this.http.get(apiItems.films.film, { params })
        .pipe(map((filmsData) => {
        return get(filmsData, ['data', 'movies', '0']);
      }));
    }))).pipe(map(data => {
      return flatten(data);
    }));
  }

  getTrailers(idIMDB: string) {
    let params = new HttpParams();
    params = params.append('idIMDB', idIMDB);
    params = params.append('trailers', '1');
    // return of(trailerMock);    // UNCOMMENT IF TRAILERS FAILED
    return this.http.get(apiItems.films.trailers, { params })
    .pipe(map((filmsData) => {
      return get(filmsData, ['data', 'movies', '0', 'trailer', 'qualities'], []);
    }));
  }

}
