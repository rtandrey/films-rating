import { Injectable } from '@angular/core';
import { compact, get } from 'lodash';
import { Subject } from 'rxjs';
// import { BsModalService } from 'ngx-bootstrap/modal';

@Injectable({
  providedIn: 'root'
})
export class BrowserStorageService {
  isFilmRemoved = new Subject();
  token = new Subject();

  // constructor(private modalService: BsModalService) {}
  constructor() {}

  getLocal(key: string): any {
    const data = window.localStorage.getItem(key);
    if (data) {
      return JSON.parse(data);
    } else {
      return null;
    }
  }
  getAllLocal(): any {
    return compact(Object.keys(window.localStorage).map(key => {
      const data = window.localStorage.getItem(key);
      if (data) {
        return JSON.parse(data);
      } else {
        return null;
      }
    }));
  }

  setLocal(key: string, value: any): void {
    const data = value === undefined ? '' : JSON.stringify(value);
    window.localStorage.setItem(key, data);
  }

  removeLocal(key: string): void {
    window.localStorage.removeItem(key);
    this.isFilmRemoved.next(true);
  }

  removeAllLocals(): void {
    for (const key in window.localStorage) {
      if (window.localStorage.hasOwnProperty(key)) {
        this.removeLocal(key);
      }
    }
  }
}
