import { HttpInterceptorFn } from '@angular/common/http';
import { apiToken } from '../../constants/api.constants';
import { formatFilmData } from '../../constants/app.constants';

export const HttpRequestInterceptor: HttpInterceptorFn = (req, next) => {

  const newReq = req.clone({
    params: req.params
        .append('token', apiToken)
        .append('format', formatFilmData.json)
  });

  return next(newReq);
}
