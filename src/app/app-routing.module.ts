import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RatingComponent } from './components/rating/rating.component';
import { SelectedTopComponent } from './components/selected-top/selected-top.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'rating',
    pathMatch: 'full',
  },
  {
    path: 'rating',
    component: RatingComponent,
  },
  {
    path: 'selected-top',
    component: SelectedTopComponent,
  },
  {
    path: 'decades',
    component: PieChartComponent,
  },
  { path: '**', redirectTo: 'rating' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
